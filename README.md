# SCoNEs repository
By Mathieu Bourgey; __Ph.D__

SCoNEs stands for **S**omatic **Co**py **N**umber **Es**timator  


SCoNEs is a R tools dedicated to estimate the Copy Number Variation in whole genome sequencing paired cancer dat using a read depth  depth approach 

SCoNEs general idea is that in cancer data each cancer and each indavidual are unique so one can't use the same detection thresholds and parameters to a whole set of sample.
So for each pair SCoNEs will detect and use the set of parameter that is the most appropriate to the sample specificity.

SCoNEs could also been aplied to non-cancer data (unpaired data) but the performance will be decreased.


## SCoNEs usage
TBD